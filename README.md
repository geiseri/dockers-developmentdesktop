# Development Desktop for Qt5

A fairly complete Qt5 development environment exposed via RDP and configured by cloud-init.
It is currently based off of "arch-linux" but all of the important items are configured
via cloud init so most user's will not notice.

## Notable Development Packages
* qt5-base 5.4
* qt5-connectivity 5.4
* qt5-declarative 5.4
* qt5-doc 5.4
* qt5-enginio 5.4
* qt5-graphicaleffects 5.4
* qt5-imageformats 5.4
* qt5-location 5.4
* qt5-multimedia 5.4
* qt5-quick1 5.4
* qt5-quickcontrols 5.4
* qt5-script 5.4
* qt5-sensors 5.4
* qt5-serialport 5.4
* qt5-svg 5.4
* qt5-tools 5.4
* qt5-translations 5.4
* qt5-wayland 5.4
* qt5-webchannel 5.4
* qt5-webengine 5.4
* qt5-webkit 5.4
* qt5-websockets 5.4
* qt5-x11extras 5.4
* qt5-xmlpatterns 5.4
* qtcreator 3.4
* gcc 5.1
* git 2.4
* xorg-xserver 1.17.1

If you want a complete list run ``pacman -Q``

# Configuration

To configure the desktop you need to create a cloud-init
[No Cloud](http://cloudinit.readthedocs.org/en/latest/topics/datasources.html#no-cloud) data store.

Currently the following modules are enabled:

* bootcmd
* write-files
* set_hostname
* update_hostname
* ca-certs
* users-groups
* ssh
* ssh-import-id
* set-passwords
* timezone
* runcmd
* scripts-vendor
* scripts-per-once
* scripts-per-boot
* scripts-per-instance
* scripts-user
* ssh-authkey-fingerprints
* keys-to-console
* phone-home
* final-message

**NOTE:** sshd is enabled, but it is only really for debugging.  
[I agree with this assessment](http://jpetazzo.github.io/2014/06/23/docker-ssh-considered-evil/) 
but to each their own.

## meta-data:

```
instance-id: iid-local01
local-hostname: cloudimg
```

## user-data:

```
#cloud-config
# vim: syntax=yaml

groups: 
  - developers
timezone: $TIMEZONE
users: 
  - gecos: "Qt Creator Session User"
    groups: 
      - developers
      - users
    lock-passwd: false
    name: $USERNAME
    passwd: $PASSWORD_HASH

# required for NLA support in RDP
runcmd:
  - "openssl req -x509 -newkey rsa:2048 -nodes -keyout /etc/xrdp/key.pem -out /etc/xrdp/cert.pem -days 365 -subj '/CN=dockerdesktop'"
```

This docker uses systemd internally so the ``--cap-add SYS_ADMIN`` and ``-v /sys/fs/cgroup:/sys/fs/cgroup:ro`` are
required to let that work.  

```
docker run -dti --name=development --cap-add SYS_ADMIN \
           -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
           -v /path/to/cloud-init:/var/lib/cloud/seed/nocloud \
           -p 3389:3389 geiseri/qt5development
```

# Building

To build the docker from scratch there are some support packages that need to be built.  The ``packages``
directory contains a script called ``buildpackages.sh``.  To run it you will need to make sure you have
``devtools`` installed on your host.  The script will then build and populate the ``repo`` directory for
the Dockerfile to use.  

