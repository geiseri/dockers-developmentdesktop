#!/bin/sh -x

BASE=$PWD
REPO=$PWD/repo
CHROOT=$BASE/chroot

LOCAL_PKGS=""
AUR_PKGS="qxmpp-qt5 qtftp-qt5 uncrustify discount cutemarked qamqp-git smtpclient-qt5-git tint2-git perl-linux-desktopfiles obmenu-generator"
MY_PKGS="x11rdp-git xrdp-git  qml-material qtsystemd qmlsystray QObjectListModel xcbsupport"

own_by_user() {
   echo $1
}

build_local() {
   PKG=$1
   own_by_user $PKG
   pushd $PKG

   makechrootpkg -u -r $CHROOT -- -i --clean && \
   mv *.pkg.tar.xz $REPO || exit 1

   popd
}


build_git() {
   PKG=$1

   WORK=$(mktemp -d -p $BASE)

   pushd $WORK
       git archive --remote=git@bitbucket.org:geiseri/arch-builds.git master $PKG | tar xvf -
       own_by_user $WORK
       build_local $PKG
   popd
   rm -rf $WORK
}

build_aur() {
   PKG=$1

   WORK=$(mktemp -d -p $BASE)
   
   pushd $WORK
       curl https://aur.archlinux.org/cgit/aur.git/snapshot/$PKG.tar.gz | tar zxvf -
       own_by_user $WORK
       build_local $PKG
   popd
   rm -rf $WORK
}

sudo rm -rf $CHROOT
mkdir -p $CHROOT
mkarchroot  $CHROOT/root base-devel || exit
arch-nspawn $CHROOT/root pacman -Syu || exit
arch-nspawn $CHROOT/root install -d -m777 /opt/X11rdp

rm -rf $REPO
mkdir -p $REPO


for PKG in $MY_PKGS
do
    build_git $PKG || exit
done

for PKG in $AUR_PKGS
do
    build_aur $PKG || exit
done

for PKG in $LOCAL_PKGS
do
    build_local $PKG || exit
done

pushd $REPO && \
    rsync -avz -e ssh --delete geiseri@geiseri.com:/home/geiseri/geiseri.com/packages/repo/ . && \
popd

pushd $REPO && \
    repo-add -R repo.db.tar.gz *.pkg.tar.xz && \
popd

pushd $REPO && \
    rsync -avz -e ssh --delete . geiseri@geiseri.com:/home/geiseri/geiseri.com/packages/repo/ && \
popd

sudo rm -rf $CHROOT $REPO
