#!/bin/bash

docker run -dti \
           --name=development \
           --cap-add SYS_ADMIN \
           -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
           -v $PWD/cloud-init:/var/lib/cloud/seed/nocloud \
           -p 3389:3389 geiseri/developmentdesktop

